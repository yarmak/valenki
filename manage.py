#!/usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
import os
import sys


if __name__ == "__main__":
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings.development')
    cwd = os.path.abspath(os.path.dirname(__file__))
    sys.path.insert(0, os.path.join(cwd, 'src'))
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
