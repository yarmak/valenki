app = angular.module 'valenki', ['ngResource', 'ngRoute', 'route-segment', 'view-segment', 'formly', 'formlyBootstrap', 'formlyDjangoRest']


app.config ['$httpProvider', ($httpProvider) ->
  $httpProvider.defaults.xsrfCookieName = 'csrftoken'
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
  return
]

app.config ["$locationProvider", ($locationProvider) ->
  $locationProvider.html5Mode true
  return
]

app.config ['$routeSegmentProvider', "$routeProvider", ($routeSegmentProvider, $routeProvider) ->

  $routeSegmentProvider
    .when "/", "base"
    .when "/work", "base.work"
    .when "/clients", "base.clients"
    .when "/projects", "base.projects"
    .when "/stages", "base.stages"

    .segment "base",
      templateUrl: "/static/html/base.html"
      controller: "BaseController"

    .within()
      .segment "work",
        default: true
        templateUrl: "/static/html/workitem/workitem_list.html"
        controller: "ListWorkItemController"
      .up()
    .within()
      .segment "clients",
        templateUrl: "/static/html/client/client_list.html"
        controller: "ListClientController"
      .up()
    .within()
      .segment "projects",
        templateUrl: "/static/html/project/project_list.html"
        controller: "ListProjectController"
      .up()
    .within()
      .segment "stages",
        templateUrl: "/static/html/stage/stage_list.html"
        controller: "ListStageController"
      .up()

  $routeProvider.otherwise redirectTo: "/"


  return

]

app.factory 'Client',  ['$resource', ($resource) ->
  $resource '/api/client/list', null, 'options': method: 'OPTIONS'
]

app.factory 'WorkItem', ['$resource', ($resource) ->
  $resource '/api/work-item/list', null, 'options': method: 'OPTIONS'
]


app.factory 'Project', ['$resource', ($resource) ->
  $resource '/api/project/list', null, 'options': method: 'OPTIONS'
]

app.factory 'Stage', ['$resource', ($resource) ->
  $resource '/api/stage/list', null, 'options': method: 'OPTIONS'
]

app.controller 'BaseController', ['$scope', ($scope) ->
  $scope.user = null
]

app.controller 'ListWorkItemController', ['$scope', 'WorkItem', '$filter', ($scope, WorkItem, $filter) ->
  $scope.workitem = WorkItem.query()

  WorkItem.options (result) ->
    $scope.fields = $filter('remake') result

  $scope.newWork = new WorkItem()
  dt = new Date()
  dt.setMilliseconds(0)
  dt.setSeconds(0)
  $scope.newWork.date_start = dt

  $scope.save = ->
    $scope.newWork.$save().then (result) ->
      $scope.workitem.push result
    .then ->
      $scope.newWork = new WorkItem()
    .then ->
      # Clear any errors
      $scope.errors = null
    , (rejection) ->
      $scope.errors = rejection.data
]


app.controller 'ListClientController', ['$scope', 'Client', '$filter', ($scope, Client, $filter) ->
  $scope.clients = Client.query()

  Client.options (result) ->
    $scope.fields = $filter('remake') result

  $scope.newClient = new Client()

  $scope.save = ->
    $scope.newClient.$save().then (result) ->
      $scope.clients.push result
    .then ->
      $scope.newClient = new Client()
    .then ->
      # Clear any errors
      $scope.errors = null
    , (rejection) ->
      $scope.errors = rejection.data
]

app.controller 'ListProjectController', ['$scope', 'Project', '$filter', ($scope, Project, $filter) ->
  $scope.projects = Project.query()

  Project.options (result) ->
    $scope.fields = $filter('remake') result

  $scope.newProject = new Project()

  $scope.save = ->
    $scope.newProject.$save().then (result) ->
      $scope.projects.push result
    .then ->
      $scope.newProject = new Project()
    .then ->
      # Clear any errors
      $scope.errors = null
    , (rejection) ->
      $scope.errors = rejection.data
]


app.controller 'ListStageController', ['$scope', '$filter',  'Stage', ($scope, $filter, Stage) ->
  $scope.stages = Stage.query()

  patch =
    value_stage:
      expressionProperties:
        "templateOptions.disabled": "model.hourly_rate"
        "templateOptions.required": "!model.hourly_rate"
    hourly_rate:
      expressionProperties:
        "templateOptions.disabled": "model.value_stage"
        "templateOptions.required": "!model.value_stage"

  Stage.options (result) ->
    $scope.fields = $filter('remake') result, patch

  $scope.newStage = new Stage()


  $scope.save = ->
    $scope.newStage.$save().then (result) ->
      $scope.stages.push result
    .then ->
      $scope.newStage = new Stage()
    .then ->
      # Clear any errors
      $scope.errors = null
    , (rejection) ->
      $scope.errors = rejection.data

  return
]
