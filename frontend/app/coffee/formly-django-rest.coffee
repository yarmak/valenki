type = (name) ->
  switch name
    when "choice", "field" then "select"
    else "input"

typeInput = (name) ->
  switch name
    when "integer" then "number"
    when "string" then "text"
    when "choice", "field" then "select"
    when "datetime" then "datetime-local"
    else "text"

get_options = (value) ->
  if value.type in ['choice', 'field']
    options = []
    angular.forEach value.choices, (option, key)->
      options.push
        name: option.display_name
        value: option.value
    return options
  return ''


angular.module 'formlyDjangoRest', []
.filter 'remake', () ->
  (input, addition) ->
    form = []
    restForm = input.actions.POST
    angular.forEach restForm, (value, key)->
      if !value.read_only
        o =
          key: key
          type: type(value.type)
          templateOptions:
            label: value.label
            required: value.required
            type: typeInput(value.type)

        if get_options(value)
          o.templateOptions.options = get_options(value)

        if addition && addition[key]
          angular.forEach addition[key], (add_value, add_key)->
            if angular.isObject(add_value)
              angular.forEach add_value, (add_sub_value, add_sub_key) ->
                o[add_key] = o[add_key] || {}
                o[add_key][add_sub_key] = add_sub_value
                console.info(add_sub_key, add_sub_value)
            else
              o[add_key] = add_value

        form.push o



    return form

