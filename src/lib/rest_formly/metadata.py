# encoding: utf-8

from __future__ import unicode_literals
from rest_framework.metadata import SimpleMetadata


class FormlyMetadata(SimpleMetadata):
    pass
