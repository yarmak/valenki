#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from project.settings.base import *
import warnings

try:
    sys.path.insert(0, rel(''))
    from settings_local import *
except ImportError:
    warnings.warn("File settings_local.py not found")
else:
    sys.path = sys.path[1:]
