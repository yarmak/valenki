#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from project.settings.base import *
import warnings


DEBUG = False
TEMPLATE_DEBUG = False


ALLOWED_HOSTS = '*'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}

