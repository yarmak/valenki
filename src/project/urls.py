# coding: utf-8

from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from django.conf import settings
from django.conf.urls import url, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.views.generic import TemplateView
from django.views.static import serve


admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('api.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^', TemplateView.as_view(template_name='base.html'))
    # url(r'^work/', include('work.urls')),
]

static = []
if settings.DEBUG:
    static += [
        url(regex=r'^%s/(?P<path>.*)$' % settings.MEDIA_URL.strip('/'),
            view=serve,
            kwargs={'document_root': settings.MEDIA_ROOT,
                    'show_indexes': True}),
    ] + staticfiles_urlpatterns()
    urlpatterns = static + urlpatterns
