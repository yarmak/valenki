# encoding: utf-8

from __future__ import unicode_literals
from rest_framework import serializers
from work.models import WorkItem, Client, Project, Stage


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client


class ProjectSerializer(serializers.ModelSerializer):
    client_name = serializers.SerializerMethodField()

    def get_client_name(self, project):
        return project.client.name

    class Meta:
        model = Project


class WorkItemSerializer(serializers.ModelSerializer):
    user_name = serializers.SerializerMethodField()
    stage_name = serializers.SerializerMethodField()

    def get_user_name(self, item):
        return item.user.username

    def get_stage_name(self, item):
        return item.stage.name

    def get_request(self):
        return self.context['request']

    def create(self, validated_data):
        validated_data.update(user=self.get_request().user)
        return super(WorkItemSerializer, self).create(validated_data)

    class Meta:
        model = WorkItem
        read_only_fields = ('user',)


class StageSerializer(serializers.ModelSerializer):
    project_name = serializers.SerializerMethodField()
    valuta_name = serializers.SerializerMethodField()

    def get_project_name(self, stage):
        return stage.project.name

    def get_valuta_name(self, item):
        return item.get_valuta_display()

    class Meta:
        model = Stage
        read_only_fields = ('is_active', )


        # write_only_fields = ('data_start', 'description', 'project')
        # fields = ('id', 'description', 'data_start', )

# class UserSerializer(serializers.ModelSerializer):
#     posts = serializers.HyperlinkedIdentityField('posts', view_name='userpost-list', lookup_field='username')
#
#     class Meta:
#         model = User
#         fields = ('id', 'username', 'first_name', 'last_name', 'posts', )
#
#
# class PostSerializer(serializers.ModelSerializer):
#     author = UserSerializer(required=False)
#     photos = serializers.HyperlinkedIdentityField('photos', view_name='postphoto-list')
#     # author = serializers.HyperlinkedRelatedField(view_name='user-detail', lookup_field='username')
#
#     def get_validation_exclusions(self, *args, **kwargs):
#         # Need to exclude `user` since we'll add that later based off the request
#         exclusions = super(PostSerializer, self).get_validation_exclusions(*args, **kwargs)
#         return exclusions + ['author']
#
#     class Meta:
#         model = Post
#
#
# class PhotoSerializer(serializers.ModelSerializer):
#     image = serializers.Field('image.url')
#
#     class Meta:
#         model = Photo
