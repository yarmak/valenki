from rest_framework import generics, permissions


from .serializers import WorkItemSerializer, ClientSerializer, ProjectSerializer, StageSerializer
from work.models import WorkItem, Client, Project, Stage


class WorkItemList(generics.ListCreateAPIView):
    queryset = WorkItem.objects.all()
    model = WorkItem
    serializer_class = WorkItemSerializer
    permission_classes = [
        permissions.IsAuthenticated,
    ]


class ClientList(generics.ListCreateAPIView):
    model = Client
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

    permission_classes = [
        permissions.IsAuthenticated,
    ]


class ProjectList(generics.ListCreateAPIView):
    model = Project
    serializer_class = ProjectSerializer
    queryset = Project.objects.all()

    permission_classes = [
        permissions.IsAuthenticated,
    ]


class StageList(generics.ListCreateAPIView):
    model = Stage
    serializer_class = StageSerializer
    queryset = Stage.objects.all()

    permission_classes = [
        permissions.IsAuthenticated,
    ]

    # def pre_save(self, obj):
    #     """Force author to the current user on save"""
    #     obj.user = self.request.user
    #     return super(WorkItemList, self).pre_save(obj)

# class UserList(generics.ListAPIView):
#     model = User
#     serializer_class = UserSerializer
#     permission_classes = [
#         permissions.AllowAny
#     ]
#
#
# class UserDetail(generics.RetrieveAPIView):
#     model = User
#     serializer_class = UserSerializer
#     lookup_field = 'username'
#
#
# class PostMixin(object):
#     model = Post
#     serializer_class = PostSerializer
#     permission_classes = [
#         PostAuthorCanEditPermission
#     ]
#
#     def pre_save(self, obj):
#         """Force author to the current user on save"""
#         obj.author = self.request.user
#         return super(PostMixin, self).pre_save(obj)
#
#
# class PostList(PostMixin, generics.ListCreateAPIView):
#     pass
#
#
# class PostDetail(PostMixin, generics.RetrieveUpdateDestroyAPIView):
#     pass
#
#
# class UserPostList(generics.ListAPIView):
#     model = Post
#     serializer_class = PostSerializer
#
#     def get_queryset(self):
#         queryset = super(UserPostList, self).get_queryset()
#         return queryset.filter(author__username=self.kwargs.get('username'))
#
#
# class PhotoList(generics.ListCreateAPIView):
#     model = Photo
#     serializer_class = PhotoSerializer
#     permission_classes = [
#         permissions.AllowAny
#     ]
#
#
# class PhotoDetail(generics.RetrieveUpdateDestroyAPIView):
#     model = Photo
#     serializer_class = PhotoSerializer
#     permission_classes = [
#         permissions.AllowAny
#     ]
#
#
# class PostPhotoList(generics.ListAPIView):
#     model = Photo
#     serializer_class = PhotoSerializer
#
#     def get_queryset(self):
#         queryset = super(PostPhotoList, self).get_queryset()
#         return queryset.filter(post__pk=self.kwargs.get('pk'))
