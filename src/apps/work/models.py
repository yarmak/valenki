# encoding: utf-8

from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models


class Client(models.Model):
    name = models.CharField(max_length=256)

    def __unicode__(self):
        return self.name


class Project(models.Model):
    client = models.ForeignKey(Client)
    name = models.CharField(max_length=256)
    site = models.URLField()

    def __unicode__(self):
        return '%s (%s) - %s' % (self.name, self.client, self.site)


class Stage(models.Model):
    RUB = 1
    USD = 2
    EUR = 3

    VALUTA_CHOICES = (
        (RUB, 'рубль'),
        (USD, 'доллар'),
        (EUR, 'евро')
    )

    project = models.ForeignKey(Project)

    name = models.CharField(max_length=256)

    value_stage = models.IntegerField('Стоимость этапа', blank=True, default=0)
    hourly_rate = models.IntegerField('Стоимость часа', blank=True, default=0)
    valuta = models.IntegerField('Валюта', choices=VALUTA_CHOICES, default=1)

    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.project.name)


class WorkItem(models.Model):
    user = models.ForeignKey(User)
    stage = models.ForeignKey(Stage)
    description = models.TextField()

    date_start = models.DateTimeField()
    date_end = models.DateTimeField(blank=True, null=True)
