# encoding: utf-8

from __future__ import unicode_literals
from django.conf.urls import url
from work.views import WorkItemListView, WorkItemAddView

# urlpatterns = [
#     url(r'^list/$', WorkItemListView.as_view(), name='work_item_list'),
#     url(r'^add/$', WorkItemAddView.as_view(), name='work_item_add'),
# ]
