# encoding: utf-8

from __future__ import unicode_literals
from work.models import Client, Stage, Project, WorkItem
from django.contrib import admin


class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', ]


class ProjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'client', 'site']


class StageAdmin(admin.ModelAdmin):
    list_display = ['name', 'project', 'value_stage', 'hourly_rate', 'valuta', 'is_active']


class WorkItemAdmin(admin.ModelAdmin):
    list_display = ['user', 'stage', 'date_start', 'date_end', 'description', ]


admin.site.register(Client, ClientAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Stage, StageAdmin)
admin.site.register(WorkItem, WorkItemAdmin)


